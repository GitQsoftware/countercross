# CounterCross

* **Site web:** https://clgdrouyn.fr/spip/spip.php?article590
* **Email:** qsoftware@protonmail.com
* **License:** GNU General Public License (version 3)
* **Contributeur:** [L Software](https://gitlab.com/KayrougeDev)

----

#### Les outils utilisés
* [Python](https://www.python.org): langage de programation
* [Qt](https://www.qt.io): outil de création et de gestion des fenêtres
* [PyQt5](https://riverbankcomputing.com): lien entre Qt et Python

#### Bibliothèque utilisées
* [Webbrowser](https://pypi.org/project/pycopy-webbrowser/): Module python pour ouvrir des pages web dans un naviguateur
* [OdsLib](https://pypi.org/project/odslib3/): Module python pour la conversion d'une table SQLite en OD.
* [Urlib3!](https://pypi.org/project/urllib3/): client HTTP pour Python
* [Subprocess](https://pypi.org/project/cubicweb-subprocess/): Module Python utilisé pour l'ouverture de l'explorateur de fichier.

### Image utilisées
* [Freepik](https://www.flaticon.com/authors/freepik): Image oeil ouvert
* [Google](https://www.flaticon.com/authors/google): Image oeil barré
* [Roundicons](https://www.flaticon.com/authors/roundicons): Image flèche, croix, entrer/sortir plein écran

### Autres

Le lien vers un article : [clgdrouyn.fr](https://clgdrouyn.fr/spip/spip.php?article590)
