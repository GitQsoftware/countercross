#!/usr/bin/env python3
# -*- coding: utf-8 -*-



# importation des modules utiles :
import sys
import os
from pathlib import Path
from PyQt5 import QtGui
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

# récupération du chemin :
HERE = os.path.dirname(os.path.abspath(__file__))
# ajout du chemin au path (+ libs) :
sys.path.insert(0, HERE)
# on démarre dans le bon dossier :
os.chdir(HERE)

import csv
import sqlite3

# on vérifie l'existence du dossier de travail et de la DB :
#DATA_DIR = Path.home() / ".countercross"
DATA_DIR = Path(QStandardPaths.writableLocation(QStandardPaths.ConfigLocation)) / "countercross"
DATA_DIR.mkdir(parents=True, exist_ok=True)
DATABASE_DIR = DATA_DIR / "cc_data.sqlite"
CSV_DIR = DATA_DIR / "eleves.csv"
if not(Path.is_file(DATABASE_DIR)):
    # si la DB n'existe pas encore, on copie celle du dossier :
    HERE = Path(HERE)
    shutil.copyfile(HERE / "cc_data.sqlite", DATABASE_DIR)

def csv2List(fileName):
    """
    lit un fichier csv et retourne son contenu sous forme de 2 listes :
        headers contient les titres (première ligne du fichier)
        data contient les autres lignes.
    """
    headers, data = [], []
    # pour gérer l'encodage du fichier :
    encodings = ('utf-8', 'iso-8859-15', 'iso-8859-1')
    for enc in encodings:
        headers, data = [], []
        firstRow = True
        theFile = open(fileName, newline='', encoding=enc)
        try:
            reader = csv.reader(
                theFile, 
                delimiter=';', 
                quotechar='"', 
                quoting=csv.QUOTE_NONNUMERIC)
            for row in reader:
                newRow = []
                for item in row:
                    if item in ('', None):
                        newRow.append('')
                    elif isinstance(item, (int, float)):
                        newRow.append(item)
                    else:
                        newRow.append(item)
                if firstRow:
                    headers = newRow
                    firstRow = False
                else:
                    data.append(newRow)
        except:
            continue
        finally:
            theFile.close()
        break
    return headers, data





"""
Colonnes de la table eleves de la base de données :
"id" INTEGER, "nom" TEXT, "prenom" TEXT, "sexe" TEXT, "niveau" INTEGER, "classe" INTEGER

Colonnes du fichier CSV exporté depuis VÉRAC :
'id', 'num', 'NOM', 'Prenom', 'Classe', 'Login', 'Date_naiss', 'Mdp', 'AnDernier', 'eleve_id', 'dateEntree', 'dateSortie', 'sexe'

Il faut donc récupérer les colonnes 'id' , 'NOM' , 'Prenom' , 'Classe' , 'sexe'.
Certaines doivent être retravaillées (sexe) ou créées (niveau).
De plus il faut tenir compte de la colonne 'dateSortie' car le fichier contient les élèves partis.
"""

def recupEleves():
    # on récupère le fichier CSV :
    fileName = DATA_DIR / 'eleves.csv'
    headers, data = csv2List(fileName)
    # il faut remettre en forme les données 
    # pour les écrire dans le fichier sqlite.
    # Donc on remplit une nouvelle liste 'dataForDB' :
    dataForDB = []
    for row in data:
        # les élèves qui ont quitté le collège en cours d'année
        # sont encore dans le fichier (avec une date de sortie).
        # Donc on ne récupère que les élèves pour lesquels 
        # la colonne 'dateSortie' contient -1 :
        if int(row[11]) < 0:
            # c'est le numéro d'inscription au collège
            # donc forcément unique :
            id = int(row[0])
            nom = row[2]
            prenom = row[3]
            # 'sexe' est 1 ou 2 dans le CSV et doit être M ou F dans sqlite :
            if int(row[12]) == 1:
                sexe = 'M'
            else:
                sexe = 'F'
            # pas de niveau dans le fichier CSV.
            # Du coup je prend la première lettre du nom de la classe
            # transformée en entier :
            niveau = int(row[4][0])
            classe = int(row[4][2])
            # PB : classe est un texte ('4°6' par exemple) :
            #classe = row[4]
            dataForDB.append((id, nom, prenom, sexe, niveau, classe))
    """
    # pour vérifier :
    for row in dataForDB:
        print(row)
    """

    # on peut remplir la base de données :
    con = sqlite3.connect(DATA_DIR / 'cc_data.sqlite')
    cur = con.cursor()
    cur.execute('DELETE FROM eleves')
    cur.executemany(
        'INSERT INTO eleves VALUES (?, ?, ?, ?, ?, ?)', dataForDB)
    con.commit()
    cur.execute('VACUUM')
    con.close()


recupEleves()


