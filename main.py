#!/usr/bin/env python3
#coding utf-8
import sys, os, time, sqlite3,shutil,webbrowser,colorama,settings
from odslib3 import ODS
from pathlib import Path
from urllib.request import urlretrieve,urlopen
from subprocess import Popen

HERE = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0,HERE)
os.chdir(HERE)

cc_version = "1.3.0"

try:
    from PyQt5 import QtGui
    from PyQt5.QtWidgets import *
    from PyQt5.QtCore import *
except:
    print("Impossible de trouver une verion compatible de PyQt5 !")
    sys.exit()

colorama.init(autoreset=True)

print("Tout a été importé avec succès !")

ASCIItitle = r'''
{0} ██████╗ ██████╗ ██╗   ██╗███╗   ██╗████████╗███████╗██████╗ {1} ██████╗██████╗  ██████╗ ███████╗███████╗
{0}██╔════╝██╔═══██╗██║   ██║████╗  ██║╚══██╔══╝██╔════╝██╔══██╗{1}██╔════╝██╔══██╗██╔═══██╗██╔════╝██╔════╝
{0}██║     ██║   ██║██║   ██║██╔██╗ ██║   ██║   █████╗  ██████╔╝{1}██║     ██████╔╝██║   ██║███████╗███████╗
{0}██║     ██║   ██║██║   ██║██║╚██╗██║   ██║   ██╔══╝  ██╔══██╗{1}██║     ██╔══██╗██║   ██║╚════██║╚════██║
{0}╚██████╗╚██████╔╝╚██████╔╝██║ ╚████║   ██║   ███████╗██║  ██║{1}╚██████╗██║  ██║╚██████╔╝███████║███████║
{0} ╚═════╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═══╝   ╚═╝   ╚══════╝╚═╝  ╚═╝{1} ╚═════╝╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚══════╝
{2}
 ██╗   ██████╗     ██████╗ 
███║   ╚════██╗   ██╔═████╗
╚██║    █████╔╝   ██║██╔██║
 ██║    ╚═══██╗   ████╔╝██║
 ██║██╗██████╔╝██╗╚██████╔╝
 ╚═╝╚═╝╚═════╝ ╚═╝ ╚═════╝'''.format(colorama.Fore.BLUE,colorama.Fore.YELLOW,colorama.Fore.LIGHTBLACK_EX)

print(ASCIItitle)

# on vérifie l'existence du dossier de travail et de la DB :
#DATA_DIR = Path.home() / ".countercross"
DATA_DIR = Path(QStandardPaths.writableLocation(QStandardPaths.ConfigLocation)) / "countercross"
DATA_DIR.mkdir(parents=True, exist_ok=True)
DATABASE_DIR = DATA_DIR / "cc_data.sqlite"
HERE = Path(HERE)
CONFIG_FILE = DATA_DIR / "config.txt"
if not(Path.is_file(DATABASE_DIR)):
    # si la DB n'existe pas encore, on copie celle du dossier :
    shutil.copyfile(HERE / "cc_data.sqlite", DATABASE_DIR)
IMG_DIR = HERE / "resource" / "img"

setting = settings.Settings(str(CONFIG_FILE), appName="CounterCross")

# Model de la table
class MyTableModel(QAbstractTableModel):
    """
    Un TableModel spécialisé pour lire les tables de la DB.
    """
    def __init__(self, parent=None):
        super(MyTableModel, self).__init__(parent)
        self.headers, self.rows = [], []

    def setTable(self, table=''):
        #print('setTable :', table)
        self.beginResetModel()
        try:
            self.headers, self.rows = [], []
            if table == 'CLIPBOARD':
                """
                la table "CLIPBOARD" n'existe pas dans la base de données.
                Cela permet de récupérer le contenu du presse-papier
                pour remplir la table "eleves".
                """
                # récupération du presse-papier :
                clipboard = QApplication.clipboard()
                #print(clipboard.text())
                clipboardData = []
                for rowDatas in clipboard.text().split('\n'):
                    clipboardData.append(rowDatas.split('\t'))
                # il peut y avoir une ligne inutile (un dernier \n) :
                if len(clipboardData[-1]) < len(clipboardData[0]):
                    clipboardData.pop()
                elif clipboardData[-1] == ['']:
                    clipboardData.pop()
                #print(clipboardData)
                self.headers = ['id', 'nom', 'prenom', 'sexe', 'niveau', 'classe']
                for line in clipboardData:
                    self.rows.append(tuple(line))
                #print(self.rows)
            else:
                conn = sqlite3.connect(str(DATABASE_DIR))
                cur = conn.cursor()
                # la commande pour récupérer les titres des colonnes :
                commandLine = 'PRAGMA table_info("{0}")'.format(table)
                cur.execute(commandLine)
                for line in cur.fetchall():
                    self.headers.append(line[1])
                # et pour récupérer les enregistrements :
                commandLine = 'select * from "{0}"'.format(table)
                cur.execute(commandLine)
                for line in cur.fetchall():
                    self.rows.append(line)
                conn.close()
        finally:
            self.endResetModel()

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        # affichage d'un titre de colonne
        if (role == Qt.DisplayRole) and (orientation == Qt.Horizontal):
            return self.headers[section]
        else:
            return

    def data(self, index, role=Qt.DisplayRole):
        # affichage d'une case
        if role == Qt.DisplayRole:
            return self.rows[index.row()][index.column()]
        elif role == Qt.TextAlignmentRole:
            return Qt.AlignCenter

    def columnCount(self, parent):
        return len(self.headers)

    def rowCount(self, parent):
        return len(self.rows)




#La fenêtre
class Window(QMainWindow): 
    def __init__(self,splash:QSplashScreen): 
        # Initialisation de la fenêtre
        super().__init__()

        # Action effectuer au démarrage
        self.onStart()

        self.classlist = []
        self.updateTableList()
        # Variable
        self.current_table = self.classlist[0] # table actuelle par defaut
        self.splash = splash # Le splashscreen
        self.icons = {
            "download":QtGui.QIcon(str(IMG_DIR/"download.png")),
            "upload":QtGui.QIcon(str(IMG_DIR/"upload.png")),
            "left":QtGui.QIcon(str(IMG_DIR/"left.png")),
            "right":QtGui.QIcon(str(IMG_DIR/"right.png")),
            "eye-close":QtGui.QIcon(str(IMG_DIR/"eye-close.png")),
            "eye-open":QtGui.QIcon(str(IMG_DIR/"eye-open.png")),
            "cross":QtGui.QIcon(str(IMG_DIR/"cross.png")),
            "expand":QtGui.QIcon(str(IMG_DIR/"expand.png")),
            "minimize":QtGui.QIcon(str(IMG_DIR/"minimize.png"))
        } # Un dictionnaire d'icon

        self.widget = QWidget()
        self.setCentralWidget(self.widget)

        #Les labels
        Label = QLabel("Bienvenue sur CounterCross",self)
        Label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.current_table_label = QLabel("Course actuelle : "+str(self.current_table),self) # Label qui dit la classe actuelle
        self.current_table_label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.enter_id_edit = QLineEdit()
        self.enter_id_edit.setValidator(QtGui.QIntValidator())
        self.enter_id_edit.setMaxLength(5)
        self.enter_id_edit.setAlignment(Qt.AlignmentFlag.AlignLeft)

        self.enter_id_button = QPushButton("Entrer le dossard",self)
        self.enter_id_button.clicked.connect(self.enterStudentId)
        self.enter_id_button.setShortcut(QtGui.QKeySequence(Qt.Key_Return))

        self.choose_db_button = QPushButton("Choisir une course",self)
        self.choose_db_button.clicked.connect(self.chooseDB)

        #Boutons
        self.bouton = QPushButton("Afficher la base de données",self)
        self.view = QTableView(self)
        self.view.setVisible(True)
        self.bouton.clicked.connect(self.toggleDB)

        self.saveChangesButton = QPushButton("Enregistrer les modifications",self)
        self.saveChangesButton.clicked.connect(self.saveChanges)
        self.saveChangesButton.setVisible(False)

        #La fenetre et ses details
        self.win_icon = QtGui.QIcon('marathon.png')
        self.setWindowIcon(self.win_icon)
        self.setWindowTitle("CounterCross " + cc_version)
        self.resize(700, 500)

        self.vbox = QVBoxLayout()
        self.vbox.setContentsMargins(25, 15, 25, 15)
        self.vbox.addWidget(Label)
        self.vbox.addWidget(self.current_table_label)
        self.vbox.addWidget(self.enter_id_edit)
        self.vbox.addWidget(self.enter_id_button)
        self.vbox.addWidget(self.choose_db_button)
        self.vbox.addWidget(self.bouton)
        self.vbox.addWidget(self.view)
        self.vbox.addWidget(self.saveChangesButton)
        self.widget.setLayout(self.vbox)

        self.dowidget()
    
    def restart(self):
        self.close()
        args = ""
        for arg in sys.argv:
            args = args+arg+" "
        Popen("python3 "+str(args))

    def importDB(self):
        newDir = QFileDialog.getOpenFileName(self,"Choisir la db",str(HERE),"sqlite (*.sqlite)")
        if newDir and not newDir[0] == '':
            shutil.copyfile(newDir[0], DATABASE_DIR)
            self.updateShowedDB()

    def getClasseMedium(self,niveau,classe) -> int:
        # On ouvre la connection avec la DB et on récupère le curseur
        conn = sqlite3.connect(DATABASE_DIR)
        cursor = conn.cursor()

        # On séléctionne tout les élèves dont le niveau et le même que celui demandé
        cursor.execute("select * from C{0}".format(niveau))
        result = cursor.fetchall()

        # Pareil que au dessus mais seulement ceux qui sont dans la classe demandée
        cursor.execute("select * from C{0} where classe={1}".format(niveau,classe))
        result2 = cursor.fetchall()

        # On créé une liste qui va contenir (grâce au 'for'): nom_prénom de tout les élèves du meme niveau
        semi_final = []
        for line in result:
            semi_final.append(str(line[1])+"_"+str(line[2]))

        # On créé le dictionnaire qui va avoir pour clé "nom_prénom" et pour valeur la place dans
        # le classement (qui pour l'instant commence a 0, sera régler un peu plus bas dans le code)
        dico = self.findStudentIndexList(semi_final)

        # On créé une liste qui va contenir la place des élèves dans le classement, (ex: 1,6,13,21)
        final = []
        # On remplie la liste grace a une boucle 'for'
        for student in result2:
            # name est égal a nom_prénom sauf que cette fois ce n'est pas tout les élèves du
            # même niveau mais de la même classe
            name = student[1]+"_"+student[2]
            # On vérifie que le dico contient l'élève
            if dico.__contains__(name):
                # Est on l'ajoute a la liste avec le +1 pour que le premier n'ai pas un score de 0
                final.append(int(dico[name])+1)

        # On récupère tout les nombres dans 'final' et on les ajoutes a medium
        medium = 0
        for i in final:
            medium += int(i)

        # On ferme la connection et le curseur
        cursor.close()
        conn.close()

        """
        On retourne medium
        """
        if final.__len__() == 0:
            return medium
        else:
            return round(medium/final.__len__())

    def getIndexWithValue(self,valueList,value) -> int:
        v = 0
        try:
            v = valueList.index(value)
        except Exception as e:
            print("Impossible de trouver l'index de la liste")
        finally:
            return v
        
    def enterStudentId(self):
        studentId = self.enter_id_edit.text()
        if studentId != "":
            # on cherche d'abord si l'élève est dans la base :
            studentData = self.findStudentInTable(studentId, 'eleves')
            if studentData == -1:
                # s'il n'y est pas :
                QMessageBox().information(
                    self,
                    "Inconnu",
                    "Le dossard entré est inconnu !")
            else:
                # s'il y est, 
                # on cherche s'il est déjà inscrit dans le classement :
                niveau = studentData[4]
                table = 'C{0}'.format(niveau)
                studentList = self.findStudentInTable(studentId, table)
                if studentList == -1:
                    # s'il n'y est pas, il faut l'ajouter :
                    conn = sqlite3.connect(str(DATABASE_DIR))
                    cur = conn.cursor()
                    print("Connexion réussie à SQLite")
                    sql = "insert into C{0} values(?, ?, ?, ?, ?)".format(niveau)
                    cur.execute(
                        sql, 
                        (studentData[0],studentData[1],studentData[2],studentData[3],studentData[5]))
                    conn.commit()
                    print("Élève ajouté !")
                    cur.close()
                    conn.close()
                    print("Connexion à SQLite fermée")
                    self.enter_id_edit.clear()
                    self.updateShowedDB()
                else:
                    # s'il y est déjà :
                    QMessageBox().warning(
                        self,
                        "Déjà dans le classement",
                        "L'élève avec le dossard {0} est déjà dans le classement !".format(studentId))
                
                
    def getStudentById(self):
        num,ok = QInputDialog.getInt(self,"Récupérer un élève","Dossard :")
        if ok:
            # on passe par une fonction dédiée :
            lines = self.queryToList('select * from eleves where id='+str(num))
            # affichage du résultat :
            if len(lines) == 0:
                QMessageBox().warning(
                    self,
                    "Récupération des infos d'un élève",
                    "Impossible de récupérer l'élève avec le dossard : "+str(num)
                )
            else:
                for line in lines:
                    QMessageBox().information(
                        self,
                        "Récupération des informations d'un élève",
                        "Dossard : {0}\nPrénom : {1}\nNom : {2}\nSexe : {3}\nClasse : {4}°{5}".format(line[0],line[1],line[2],line[3],line[4],line[5])
                    )

    def chooseDB(self): # la boite de dialog pour ouvrir la base de donner
        items = []
        for i in self.classlist:
            if i == self.classlist[0]:
                items.append("Tout les participants")
            else:
                items.append(str(i))
        items = tuple(items)
        item, okPressed = QInputDialog.getItem(self, "Course","Choisissez la classe à ouvrir", items, 0, False)
        if okPressed and item:
            try:
                self.current_table = int(item)
            except:
                self.current_table = self.classlist[0]
            self.updateShowedDB()
        elif not okPressed:
            self.updateShowedDB()

    def nextDB(self): # passez a la table suivante: 6 à 5, 5 à 4, 4 à 3, 3 à 6, etc
        if self.current_table == self.classlist[len(self.classlist)-1]:
            self.current_table = self.classlist[0]
        else:
            self.current_table = self.current_table+1

        if not self.classlist.__contains__(self.current_table):
            self.nextDB()
        else:
            self.updateShowedDB()

    def updateShowedDB(self):
        self.updateTableList()
        self.saveChangesButton.setVisible(False)
        if self.current_table == self.classlist[0]:
            self.model.setTable('eleves')
            self.current_table_label.setText("Course actuelle : Tout les participants")
        else:
            self.model.setTable('C{0}'.format(self.current_table))
            self.current_table_label.setText("Course actuelle : "+str(self.current_table))

    def updateTableList(self):
        conn = sqlite3.connect(str(DATABASE_DIR))
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM sqlite_master where type='table'")
        self.classlist.clear()
        for i in cursor.fetchall():
            if i[1][0] == "C":
                self.classlist.append(int(i[1][1:]))
        self.classlist.sort()
        self.classlist.append(self.classlist[0]-1)
        self.classlist.sort()
        cursor.close()
        conn.close()

    def previousDB(self): # passez a la table précdente: 3 à 4, 4 à 5, 5 à 6, 6 à 3, etc
        if self.current_table == self.classlist[0]:
            self.current_table = self.classlist[len(self.classlist)-1]
        else:
            self.current_table = self.current_table-1

        if not self.classlist.__contains__(self.current_table):
            self.previousDB()
        else:
            self.updateShowedDB()

    def toggleDB(self): # affiche ou cache la db
        if not(self.view.isVisible()):
            self.view.setVisible(True)
            self.bouton.setText("Cacher la base de données")
            self.display_db_bar.setText("Cacher la base de données")
            self.display_db_bar.setIcon(self.icons["eye-close"])
        else:
            self.view.setVisible(False)
            self.bouton.setText("Afficher la base de données")
            self.display_db_bar.setText("Afficher la base de données")
            self.display_db_bar.setIcon(self.icons["eye-open"])

    def aboutMSG(self): # affiche le message de about
        text = "<center>" \
           "<h1>CounterCross</h1>" \
           "&#8291;" \
           "<img src=\"marathon.png\" width=\"250\" height=\"250\">" \
           "<br>" \
           "Version: "+cc_version+"<br/>" \
           "2021-2024 QL Software<br/>" \
           "Licence GNU GPL 3" \
           "</center>"
        about_dialog = QMessageBox(self)
        about_dialog.setWindowTitle(self.windowTitle())
        about_dialog.setText(text)
        about_dialog.exec()

    def read_changelog(self):
        self.read_changelog_index(1)
        
    def read_changelog_index(self,index:int):
        try:
            page=urlopen('https://gitlab.com/GitQsoftware/countercross/-/raw/main/changelog.txt')
            version_list = str(page.read().decode("utf-8")).split("_")
            self.msg_changelog = QMessageBox()
            self.msg_changelog.setIcon(QMessageBox.Information)
            self.msg_changelog.setWindowIcon(self.win_icon)
            self.msg_changelog.setWindowTitle("Nouveautés")
            self.msg_changelog.setStandardButtons(QMessageBox.Ok)
            self.msg_changelog.addButton("Voir plus de nouveautés",self.msg_changelog.ButtonRole.NoRole)
            self.msg_changelog.buttonClicked.connect(self.choose_changelog)
            
            if index >= len(version_list):
                self.msg_changelog.setText("Nous vous montrons la denière nouveauté car votre version nous est inconnue:\n\n"+str(version_list[1][0:-2]))
                self.msg_changelog.exec_()
            else:
                version_text = str(version_list[index][0:-2])
                if version_text[11:16] == str(cc_version):
                    self.msg_changelog.setText(version_text)
                    self.msg_changelog.exec_()
                else:
                    self.read_changelog_index(index+1)
        except Exception as e:
            if str(e) == "<urlopen error [Errno 11001] getaddrinfo failed>":
                e = "No internet connection"
            QMessageBox.warning(self,"Nouveautés","Impossible de récupérer les nouveautés:\n"+str(e))

    def choose_changelog(self,i):
        if i.text() == "Voir plus de nouveautés":
            self.msg_changelog.close()
            items = ()
            version_list = str(urlopen('https://gitlab.com/GitQsoftware/countercross/-/raw/main/changelog.txt').read().decode("utf-8")).split("_")
            for v in version_list:
                if v != "":
                    items = items.__add__((str(v[0:-1])[11:16],))
            #items = items.__add__(("Les 7 dernières",))
            item, okPressed = QInputDialog.getItem(self, "Classe","Choisissez la version", items, 0, False)
            if okPressed and item:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Information)
                msg.setWindowIcon(self.win_icon)
                msg.setWindowTitle("Nouveautés")
                msg.setStandardButtons(QMessageBox.Ok)
                if item == "Les 7 dernières":
                    s = ""
                    for v in version_list:
                        if s.count("_") < 7:
                            s=s+v
                    msg.setText(s)
                    msg.exec_()
                else:
                    def check(index:int):
                        if index >= len(version_list):
                            QMessageBox.information(self,"Erreur","Une erreur est survenue !")
                        else:
                            version_text = str(version_list[index][0:-1])
                            if version_text[11:16] == str(item):
                                msg.setText(version_text)
                                msg.exec_()
                            else:
                                check(index+1)
                    check(1)




    def queryToList(self, commandLine) -> list:
        """
        une fonction générale pour traiter toutes les requêtes du type "select".
        Interroge la base avec le paramètre commandLine
        et retourne une liste (lines) dont les éléments sont 
        les lignes du résultat de la requête.
        Chaque élément de la liste lines est lui-même une liste
        correspondant à une ligne (line).
        """
        lines = []
        conn = sqlite3.connect(str(DATABASE_DIR))
        cur = conn.cursor()
        cur.execute(commandLine)
        lines = cur.fetchall()
        cur.close()
        conn.close()
        return lines

    def findStudentInTable(self, studentId, table) -> int:
        """
        """
        conn = sqlite3.connect(str(DATABASE_DIR))
        cur = conn.cursor()
        commandLine = 'SELECT * FROM {0} WHERE id={1}'.format(table, studentId)
        cur.execute(commandLine)
        lines = cur.fetchall()
        conn.close()
        if len(lines) > 0:
            return lines[0]
        else:
            return -1

    def findStudentIndexList(self,studentNameList):
        final = {}
        for l in studentNameList:
            final.update({str(l):str(self.getIndexWithValue(studentNameList,l))})
        return final


    def openCredits(self): # ouvre les credits
        """page = QWebEnginePage()
        page.setUrl(QUrl("http://google.fr"))
        view = QWebEngineView()
        view.setPage(page)
        view.show()"""
        webbrowser.open('http://qlsoftware.rf.gd/docu/countercross/#credit')
        
    def print_table(self):
        conn = sqlite3.connect(str(DATABASE_DIR))
        cur = conn.cursor()
        print("Connexion réussie à SQLite")

        items = []
        for i in self.classlist:
            if i != self.classlist[0]:
                items.append(str(i))
        items = tuple(items)
        itemC, okPressed = QInputDialog.getItem(self, "Classe","Choisissez la classe pour le classement", items, 0, False)
        if okPressed and itemC:
            items = ("Fille","Garçon","Les Deux")
            item, okPressed = QInputDialog.getItem(self, "Classe","Choisissez la classe pour le classement", items, 0, False)
            if okPressed and item:
                if item == "Fille":
                    sql = "SELECT ALL * FROM C{0} WHERE sexe='F'".format(str(itemC))
                elif item == "Garçon":
                    sql = "SELECT ALL * FROM C{0} WHERE sexe='M'".format(str(itemC))
                else:
                    sql = "SELECT ALL * FROM C"+str(itemC)
                cur.execute(sql)
                res = cur.fetchall()

                # Create the document
                doc = ODS()

                # Single Cell
                doc.content.getCell(0, 0).stringValue("Dossard").setBorder()
                doc.content.getCell(1, 0).stringValue("Nom").setBorder()
                doc.content.getCell(2, 0).stringValue("Prénom").setBorder()
                doc.content.getCell(3, 0).stringValue("Sexe").setBorder()

                line = 0
                for row in res:
                    line += 1
                    doc.content.getCell(0, line).stringValue(row[0]).setBorder()
                    doc.content.getCell(1, line).stringValue(row[1]).setBorder()
                    doc.content.getCell(2, line).stringValue(row[2]).setBorder()
                    doc.content.getCell(3, line).stringValue(row[3]).setBorder()

                if not item == "Fille" and not item == "Garçon":
                    fname = QFileDialog.getSaveFileName(self, 'Sauvegarder le classement ({0})'.format(str(item)), str(settings.value.get("lastDir")), "Classeur LibreOffice (*.ods)")
                else:
                    fname = QFileDialog.getSaveFileName(self, 'Sauvegarder le classement ({0}ème)'.format(str(item)), str(settings.value.get("lastDir")), "Classeur LibreOffice (*.ods)")
                
                # Write out the document
                if fname[0] != "":
                    if fname[0][-4:] == ".ods":
                        doc.save(fname[0])
                    else:
                        doc.save(fname[0]+".ods")
                    settings.value.set("lastDir", str(fname[0]))
                    setting.saveOptions()
        cur.close()
        conn.close()
        print("Connexion SQLite est fermée")

    def delete_race(self):
        items = []
        for i in self.classlist:
            if i != self.classlist[0]:
                items.append(str(i))
        items = tuple(items)
        item, okPressed = QInputDialog.getItem(self, "Classe","Choisissez la classe pour supprimer le classement", items, 0, False)
        if okPressed and item:
            conn = sqlite3.connect(str(DATABASE_DIR))
            cur = conn.cursor()
            print("Connexion réussie à SQLite")
            sql = "DELETE FROM 'C{0}'".format(item)
            cur.execute(sql)
            conn.commit()
            cur.close()
            conn.close()
            print("Connexion à SQLite fermée")
            self.updateShowedDB()
            QMessageBox().information(
                self,
                "Classement supprimé",
                "Le classement a été correctement supprimé !"
            )

    def open_data_folder(self):
        url = QUrl().fromLocalFile(str(DATA_DIR))
        QtGui.QDesktopServices.openUrl(url)

    def usersFromClipboard(self):
        """
        on sélectionne la table "eleves", 
        on la remplit avec le contenu du presse-papier 
        et on affiche le bouton "enregistrer".
        """
        self.current_table = 0
        self.updateShowedDB()
        self.view.setVisible(True)
        self.model.setTable('CLIPBOARD')
        self.saveChangesButton.setVisible(True)

    def saveChanges(self):
        # on peut masquer le bouton "enregistrer" :
        self.saveChangesButton.setVisible(False)
        # on remplit la base de données :
        conn = sqlite3.connect(str(DATABASE_DIR))
        cur = conn.cursor()
        cur.execute('DELETE FROM eleves')
        cur.executemany(
            'INSERT INTO eleves VALUES (?, ?, ?, ?, ?, ?)', 
            self.model.rows)
        conn.commit()
        cur.execute('VACUUM')
        conn.close()

    def getClasseMediumMsgBox(self):
        items = []
        for i in self.classlist:
            if i != self.classlist[0]:
                items.append(str(i))
        items = tuple(items)
        item, okPressed = QInputDialog.getItem(self, "Classe","Choisissez la course ", items, 0, False)
        if okPressed and item:
            s = ""
            c = sqlite3.connect(DATABASE_DIR)
            cu = c.cursor()
            
            cu.execute("SELECT * FROM C{0}".format(item))
            result = cu.fetchall()
            
            cu.close()
            c.close()
                
            current = 0
            maxT = 0
            for i in result:
                current = int(i[4])
                if current > maxT:
                    maxT = current
            
            finalL = []

            for i in range(1,maxT+1):
                finalL.append(
                    (int(self.getClasseMedium(item, i)), item + "°" + str(i)))

            # Create the document
            doc = ODS()

            # Single Cell
            doc.content.getCell(0, 0).stringValue("Classes").setBorder()
            doc.content.getCell(1, 0).stringValue("Points").setBorder()

            #print(finalL)
            finalL.sort()
            #print(finalL)

            line = 0
            for (points, name) in finalL:
                line += 1
                doc.content.getCell(0, line).stringValue(name).setBorder()
                doc.content.getCell(1, line).stringValue(str(points)).setBorder()
                print("Valeur " + str(line) + ": " + name + " - " + str(points))

            fname = QFileDialog.getSaveFileName(self, 'Sauvegarder le classement par classes de la course des {0}ème'.format(str(item)), str(settings.value.get("lastDir")), "Classeur LibreOffice (*.ods)")
                
            # Write out the document
            if(fname[0] != ''):
                if fname[0][-4:] == ".ods":
                    doc.save(fname[0])
                else:
                    doc.save(fname[0]+".ods")
                settings.value.set("lastDir", str(fname[0]))
                setting.saveOptions()

    def toggleFulscreen(self):
        if self.isFullScreen():
            self.fullscreen_bar.setText("&Activer plein écran")
            self.setWindowState(Qt.WindowState.WindowNoState)
            self.fullscreen_bar.setIcon(self.icons["expand"])
        else:
            self.fullscreen_bar.setText("&Désactiver plein écran")
            self.setWindowState(Qt.WindowState.WindowFullScreen)
            self.fullscreen_bar.setIcon(self.icons["minimize"])
            
    def checkForUpdate(self):
        try:
            page=urlopen('https://gitlab.com/GitQsoftware/countercross/-/raw/main/changelog.txt')
            version_list = str(page.read().decode("utf-8")).split("_")
            version_text = str(version_list[1][0:-2])
            if version_text[11:16] != str(cc_version):
                self.splash.finish(self)
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Information)
                msg.setText("La version "+version_text[11:16]+" de CounterCross est disponible, vous avez la version "+str(cc_version))
                msg.setWindowTitle("La version "+version_text[11:16]+" est disponible")
                msg.setStandardButtons(QMessageBox.Ok)
                msg.addButton("Mettre à Jour",msg.ButtonRole.NoRole)
                def updateCC(b):
                    if b.text() == "Mettre à Jour":
                        self.updateCC(version_text[11:16])
                msg.buttonClicked.connect(updateCC)
                msg.exec_()
            else:
                print("No Update Found")
        except Exception as e:
            if str(e) == "<urlopen error [Errno 11001] getaddrinfo failed>":
                e = "No internet connection"
            print("Can't update CounterCross because:\n"+str(e))
            
    def updateCC(self,versionText:str):
        try:
            urlretrieve('https://gitlab.com/GitQsoftware/countercross/-/raw/main/main.py', HERE/__file__)
            self.restart()
        except Exception as e:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setText("Nous n'avons pas pu récupérer la dernière version de CounterCross\n"+str(e))
            msg.setInformativeText("êtes-vous connecté à internet ?")
            msg.setWindowTitle("Téléchargement impossible")
            msg.addButton("Annuler",msg.ButtonRole.NoRole)
            msg.exec_()
    
    def importConfigFile(self):
        QMessageBox.information(self,"Non disponible","La fonction demander n'est pas encore disponible !")

    def resetConfigFile(self):
        for k in settings.KEY:
            settings.value.set(str(k), settings.VALUE[settings.KEY.index(k)])
        setting.saveOptions()
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("Un redémarrage est très fortement conseillé pour éviter tout problème appuyez sur Ok pour redémarrer")
        msg.setWindowTitle("Redémarrage")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.addButton("Plus Tard",msg.ButtonRole.NoRole)
        def process(i):
            if i.text() != "Plus Tard":
                self.restart()
        msg.buttonClicked.connect(process)
        msg.exec_()

    def onCheckUpdateSettingUpdate(self,b):
        settings.value.set("CheckUpdateOnStart", int(b))
        setting.saveOptions()

    def addClass(self):
        num,ok = QInputDialog.getInt(self,"Créer une classe","Numéro :")
        if ok:
            if self.classlist.__contains__(int(num)):
                QMessageBox().warning(
                    self,
                    "Classe existante",
                    "Cette classe existe déjà !")
            else:
                if int(num) < 0:
                    QMessageBox().warning(
                        self,
                        "Classe négative",
                        "Le numéro de classe ne peut pas être négatif !")
                else:
                    conn = sqlite3.connect(str(DATABASE_DIR))
                    cur = conn.cursor()
                    print("Connexion réussie à SQLite")
                    sql = 'CREATE TABLE "C{0}" ("id"	INTEGER,"nom"	TEXT,"prenom"	TEXT,"sexe"	INTEGER,"classe"	NUMERIC)'.format(str(num))
                    conn.execute(sql)
                    print("Classe créer !")
                    cur.close()
                    conn.close()
                    self.updateTableList()
                    print(self.classlist)


    def removeClass(self):
        items = []
        for i in self.classlist:
            if i != self.classlist[0]:
                items.append(str(i))
        items = tuple(items)
        item, okPressed = QInputDialog.getItem(self, "Classe","Choisissez une classe", items, 0, False)
        if okPressed and item:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setText("Supprimer cette classe supprimer le classement est vous obligera à la recrée pour l'utiliser !")
            msg.setWindowTitle("Supprimer la classe")
            msg.setStandardButtons(QMessageBox.Cancel)
            msg.addButton("Supprimer",msg.ButtonRole.NoRole)
            def rmvClass(b):
                if b.text() == "Supprimer":
                    conn = sqlite3.connect(str(DATABASE_DIR))
                    cur = conn.cursor()
                    print("Connexion réussie à SQLite")
                    sql = "DROP TABLE C{0}".format(str(item))
                    conn.execute(sql)
                    print("Classe supprimer !")
                    cur.close()
                    conn.close()
                    self.current_table = self.classlist[0]
                    self.updateTableList()
            msg.buttonClicked.connect(rmvClass)
            msg.exec_()
            

    def dowidget(self):
        #Les menu fichier
        self.menubar = self.menuBar()
        self.fileMenu = self.menubar.addMenu("&Fichier")  # Add Help in menu bar

        self.display_db_bar = self.fileMenu.addAction("&Afficher la base de données")  # Afficher / Cacher db
        self.display_db_bar.setShortcut("Ctrl+Shift+D")
        self.display_db_bar.triggered.connect(self.toggleDB)
        self.display_db_bar.setIcon(self.icons["eye-open"])

        self.delete_race_bar = self.fileMenu.addAction("&Supprimer un classement")  # Afficher / Cacher db
        self.delete_race_bar.setShortcut("Ctrl+D")
        self.delete_race_bar.triggered.connect(self.delete_race)
        self.delete_race_bar.setIcon(self.icons["cross"])

        self.fileMenu.addSeparator()

        self.users_from_clipboard = self.fileMenu.addAction("&Importer les utilisateurs")
        self.users_from_clipboard.setShortcut("Ctrl+V")
        self.users_from_clipboard.triggered.connect(self.usersFromClipboard)

        self.import_db = self.fileMenu.addAction("Importer un base de données")
        self.import_db.setShortcut("Ctrl+O")
        self.import_db.triggered.connect(self.importDB)
        self.import_db.setIcon(self.icons["download"])

        self.show_data_inexplorer = self.fileMenu.addAction("Afficher le dossier des données")
        self.show_data_inexplorer.setShortcut("Ctrl+E")
        self.show_data_inexplorer.triggered.connect(self.open_data_folder)

        self.fileMenu.addSeparator()

        self.reset_config_file_bar = self.fileMenu.addAction("&Renitializer le fichier de config")
        self.reset_config_file_bar.triggered.connect(self.resetConfigFile)

        self.import_config_file_bar = self.fileMenu.addAction("&Importer un fichier de config")
        self.import_config_file_bar.triggered.connect(self.importConfigFile)
        
        self.fileMenu.addSeparator()

        self.choose_db = self.fileMenu.addAction("&Choisir une course")  # Afficher la prochaine classe
        self.choose_db.setShortcut("Ctrl+Shift+C")
        self.choose_db.triggered.connect(self.chooseDB)

        self.next_db = self.fileMenu.addAction("&Course suivante")  # Afficher la prochaine classe
        self.next_db.setShortcut("Ctrl+Shift+N")
        self.next_db.triggered.connect(self.nextDB)
        self.next_db.setIcon(self.icons["right"])

        self.prev_db = self.fileMenu.addAction("&Course précédente")  # Afficher la classe précédente
        self.prev_db.setShortcut("Ctrl+Shift+P")
        self.prev_db.triggered.connect(self.previousDB)
        self.prev_db.setIcon(self.icons["left"])

        self.fileMenu.addSeparator()

        self.get_student_by_id_menubar = self.fileMenu.addAction("Récupérer un élève avec son dossard")
        self.get_student_by_id_menubar.setShortcut("Ctrl+Shift+G")
        self.get_student_by_id_menubar.triggered.connect(self.getStudentById)

        self.fileMenu.addSeparator()
        
        self.get_medium_bar = self.fileMenu.addAction("Voir le classement par classes")
        self.get_medium_bar.triggered.connect(self.getClasseMediumMsgBox)
        
        self.save_ods_file = self.fileMenu.addAction("Sauvegarder en fichier ODS")
        self.save_ods_file.setShortcut("Ctrl+Shift+S")
        self.save_ods_file.triggered.connect(self.print_table)
        self.save_ods_file.setIcon(self.icons["upload"])

        self.fileMenu.addSeparator()

        self.fullscreen_bar = self.fileMenu.addAction("&Activer plein écran")
        self.fullscreen_bar.setShortcut("F11")
        self.fullscreen_bar.setCheckable(True)
        self.fullscreen_bar.triggered.connect(self.toggleFulscreen)
        self.fullscreen_bar.setIcon(self.icons["expand"])

        self.checkupdate_bar = self.fileMenu.addAction("&Chercher une mise à jour")
        self.checkupdate_bar.triggered.connect(self.checkForUpdate)

        self.quit = self.fileMenu.addAction("&Quitter")  # Quitter
        self.quit.setShortcut("Ctrl+Q")
        self.quit.triggered.connect(QtGui.QGuiApplication.quit)
        self.quit.setIcon(self.win_icon)

        #Fin menu fichier

        # Option
        self.classeMenu = self.menubar.addMenu("&Classe")
        self.addClassMenu = self.classeMenu.addAction("&Ajouter une classe") # Add another option in Help
        self.addClassMenu.triggered.connect(self.addClass)

        self.removeClassMenu = self.classeMenu.addAction("&Supprimer une classe") # Add another option in Help
        self.removeClassMenu.triggered.connect(self.removeClass)

        # Option
        self.settingMenu = self.menubar.addMenu("&Option")

        self.settingCheckUpdate = self.settingMenu.addAction("&Chercher les mises à jours au démarrage")
        self.settingCheckUpdate.setCheckable(True)
        self.settingCheckUpdate.setChecked(bool(int(settings.value.get("CheckUpdateOnStart"))))
        self.settingCheckUpdate.triggered.connect(self.onCheckUpdateSettingUpdate)
        
        #Autre
        self.otherMenu = self.menubar.addMenu("&Autre")  # Add Help in menu bar

        self.about = self.otherMenu.addAction("&À Propos...")  # Add option in Help
        self.about.setShortcut("F10") # display F11 as shortcut
        self.about_label = None
        self.about.triggered.connect(self.aboutMSG)

        self.credits = self.otherMenu.addAction("&Crédits") # Add another option in Help
        self.credits.triggered.connect(self.openCredits)

        self.readChangelog = self.otherMenu.addAction("&Lire les nouveautés") # Add another option in Help
        self.readChangelog.setShortcut("F12")
        self.readChangelog.triggered.connect(self.read_changelog)
        #Fin autre

        self.onStarted()
        
        # lecture  et affichage de la DB
        self.model = MyTableModel(self)
        self.view.setModel(self.model)

        self.updateShowedDB()

    def onStarted(self):
        # Après avoir démarrer

        #vérifier les mises a jour
        if bool(int(settings.value.get("CheckUpdateOnStart"))):
            self.checkForUpdate()
        self.splash.finish(self)
        self.show()
        
    def onStart(self):
        # Au démarrage
        if sys.argv.__contains__("-u"):
            self.resetConfigFile()
            self.read_changelog()


# Création de l'application
app = QApplication(sys.argv)

# Création du pixmap du splashscreen
splash_pix = QtGui.QPixmap("marathon.png")

# Création du splashscreen
splash = QSplashScreen(splash_pix, Qt.WindowType.WindowStaysOnTopHint)
splash.setMask(splash_pix.mask())
# Affichage du splash
splash.show()
app.processEvents()
time.sleep(0.2) # Atteindre un peu avant la...
# ...création de la fenêtre
window = Window(splash)
# On dit au programme de s'éteindre en lui donner un code de sortie qui est envoyer par le démarrage de l'app
sys.exit(app.exec())