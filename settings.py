import os


# List of the setting and the defaut value
KEY = ["CheckUpdateOnStart", "lastDir"]
VALUE = [1, ""]

class Value():
    def __init__(self):
        for v in KEY:
            globals()[v] = VALUE[KEY.index(v)]

    def get(self, setting:str):
        return globals()[setting]

    def set(self, setting:str, value):
        # If put new setting name the new settings will not be save in the settings file
        globals()[setting] = value

value = Value()

class Settings:
    def __init__(self, optionsFile, appName="App"):
        self.optionsFile = optionsFile
        self.appName = appName
        self.loadOptions()
    
    def loadOptions(self):
        try:
            if not os.path.exists(self.optionsFile) or len(open(self.optionsFile, "r").readlines()) < (len(KEY)+1):
                self.saveOptions()
            
            read = open(self.optionsFile, "r")
            for s in read.readlines():
                try:
                    content = s.split(":")
                    if len(content) == 2:
                        value.set(content[0], content[1].removesuffix("\n"))
                except Exception as e:
                    print("Error while update setting variable with file value: " + str(e))
            read.close()
        except Exception as e:
            print("Error while reading option file: " + str(e))

    def saveOptions(self):
        try:
            print = open(self.optionsFile, "w+")
            
            print.write(str(self.appName)+"'s settings\n")
            for k in KEY:
                i = KEY.index(k)
                if i != len(KEY)-1:
                    print.write(str(KEY[i])+":"+str(value.get(KEY[i]))+"\n")
                else:
                    print.write(str(KEY[i])+":"+str(value.get(KEY[i])))
            print.close()
        except Exception as e:
            print("Error while save options: " + str(e))
